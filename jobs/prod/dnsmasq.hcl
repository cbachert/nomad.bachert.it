job "dnsmasq" {
  region      = "global"
  datacenters = ["dc1"]
  type        = "system"

  group "dnsmasq" {
    task "dnsmasq" {
      driver = "docker"

      config {
        image        = "andyshinn/dnsmasq:2.81"
        network_mode = "host"
        args  = [
          "-k",
          "--listen-address=::1",
          "--bind-interfaces",
          "--server=/consul/::1#8600",
        ]
      }

      resources {
        cpu    = 20
        memory = 10

        network {
          port "dns" {
            static = 53
          }
        }
      }
    }
  }
}