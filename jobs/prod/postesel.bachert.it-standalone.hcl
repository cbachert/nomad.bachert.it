job "postesel.bachert.it" {
  region      = "global"
  datacenters = ["dc1"]
  type        = "service"
  
  group "mailu" {
    count = 1

    ephemeral_disk {
      size      = "16"
    }
    
    volume "mailu-certs" {
      type      = "host"
      read_only = true
      source    = "traefik-acme-export"
    }
    volume "mailu-redis" {
      type      = "host"
      read_only = false
      source    = "postesel.bachert.it-mailu-redis"
    }
    volume "mailu-data" {
      type      = "host"
      read_only = false
      source    = "postesel.bachert.it-mailu-data"
    }
    volume "mailu-dkim" {
      type      = "host"
      read_only = false
      source    = "postesel.bachert.it-mailu-dkim"
    }
    volume "mailu-mail" {
      type      = "host"
      read_only = false
      source    = "postesel.bachert.it-mailu-mail"
    }
    volume "mailu-filter" {
      type      = "host"
      read_only = false
      source    = "postesel.bachert.it-mailu-filter"
    }
    volume "hcloud-dnsapi" {
      type      = "host"
      read_only = true
      source    = "hcloud-dnsapi"
    }

    network {
      mode = "bridge"
      #port "front_smtp" { to = 25 }
      port "front_smtp" { 
        static = 25
        to = 25
        host_network = "publicv4"
      }
      port "front_http" { to = 80 }
      #port "front_imap" { to = 143 }
      port "front_imap" {
        static = 143
        to = 143
        host_network = "public"
      }
      #port "front_smtp_submission" { to = 587 }
      port "front_smtp_submission" {
        static = 587
        to = 587
        host_network = "public"
      }
      port "imap_imap" { to = 143 }
      port "imap_lmtp" { to = 2525 }
      port "smtp_smtp" { to = 25 }
      port "smtp_authsmtp" { to = 10025 }
      port "admin_http" { to = 80 }
      port "redis" { to = 6379 }
      port "rspamd_milter" { to = 11332 }
      port "rspamd_http" { to = 11334 }
      port "rspamd_fuzzy" { to = 11335 }
      dns {
        servers = [ "[2001:1608:10:25::1c04:b12f]", "[2001:1608:10:25::9249:d69b]", "84.200.69.80", "84.200.70.40" ]
      }
    }

    task "hcloud-dnslb-mx01" {
      driver = "docker"
      
      volume_mount {
        volume      = "hcloud-dnsapi"
        destination = "/opt/hcloud-dnsapi"
        read_only   = true
      }

      logs {
        max_files     = 1
        max_file_size = 1
      }

      config {
        image = "cbachert/py3alp-requests"
        network_mode = "host"
        command = "/opt/hcloud-dnsapi/hcloud-dnsapi-update.py"
      }

      resources {
        cpu    = 100
        memory = 16
      }

      lifecycle {
        hook    = "prestart"
      }

      env {
        HCLOUD_DNSAPI_UPDATE_ZONE_NAME = "bachert.it"
        HCLOUD_DNSAPI_UPDATE_RECORD_NAMES = "mx01"
        HCLOUD_DNSAPI_UPDATE_RECORD_TYPES = "A"
      }
    }

    task "hcloud-dnslb-postesel" {
      driver = "docker"
      
      volume_mount {
        volume      = "hcloud-dnsapi"
        destination = "/opt/hcloud-dnsapi"
        read_only   = true
      }

      logs {
        max_files     = 1
        max_file_size = 1
      }

      config {
        image = "cbachert/py3alp-requests"
        network_mode = "host"
        command = "/opt/hcloud-dnsapi/hcloud-dnsapi-update.py"
      }

      resources {
        cpu    = 100
        memory = 16
      }

      lifecycle {
        hook    = "prestart"
      }

      env {
        HCLOUD_DNSAPI_UPDATE_ZONE_NAME = "bachert.it"
        HCLOUD_DNSAPI_UPDATE_RECORD_NAMES = "postesel"
        HCLOUD_DNSAPI_UPDATE_RECORD_TYPES = "AAAA"
      }
    }

    task "docker-ipv6nat" {   # docker-ipv6nat is required to prevent open relay
      driver = "docker"

      logs {
        max_files     = 1
        max_file_size = 1
      }

      config {
        image        = "robbertkl/ipv6nat:0.4.2"
        #--privileged -v /var/run/docker.sock:/var/run/docker.sock:ro -v /lib/modules:/lib/modules:ro robbertkl/ipv6nat
        cap_add = [
          "NET_ADMIN",
          # default:
          # "NET_RAW",
          # only required if ip6_tables is not loaded automatically:
          #"SYS_MODULE",
        ]
        network_mode = "host"

        mounts = [
          {
            type = "bind"
            target = "/var/run/docker.sock"
            source = "/var/run/docker.sock"
            readonly = true
            #bind_options {
            #  propagation = "rshared"
            #}
          },
        ]
      }

      resources {
        cpu    = 100
        memory = 32
      }
    }

    task "redis" {
      driver = "docker"
      
      volume_mount {
        volume      = "mailu-redis"
        destination = "/data"
        read_only   = false
      }

      logs {
        max_files     = 1
        max_file_size = 1
      }

      config {
        image = "redis:alpine"
        ports = [ "redis" ]
        network_mode = "bridge"
      }

      resources {
        cpu    = 100
        memory = 10
      }
    }

    task "admin" {
      driver = "docker"

      # depends_on:
      #   - redis
      
      volume_mount {
        volume      = "mailu-data"
        destination = "/data"
        read_only   = false
      }
      volume_mount {
        volume      = "mailu-dkim"
        destination = "/dkim"
        read_only   = false
      }

      logs {
        max_files     = 1
        max_file_size = 1
      }
      
      config {
        image = "cbachert/mailu-admin"
        ports = [ "admin_http" ]
        network_mode = "bridge"
      }

      resources {
        cpu    = 100
        memory = 224
      }

      env {
        INITIAL_ADMIN_ACCOUNT = "root"
        INITIAL_ADMIN_DOMAIN = "bachert.it"
        INITIAL_ADMIN_PW = "password"

        IMAP_ADDRESS = "${NOMAD_IP_imap_imap}:${NOMAD_HOST_PORT_imap_imap}"
        AUTHSMTP_ADDRESS = "${NOMAD_IP_smtp_authsmtp}:${NOMAD_HOST_PORT_smtp_authsmtp}"
        SMTP_ADDRESS = "${NOMAD_IP_smtp_smtp}:${NOMAD_HOST_PORT_smtp_smtp}"
        REDIS_ADDRESS = "[${NOMAD_IP_redis}]:${NOMAD_HOST_PORT_redis}"
        POP3_ADDRESS = "127.0.0.1"   # POP3 not used, but container requires setting env variable

        #SECRET_KEY = ""
        SUBNET = "172.17.0.0/16"
        SUBNET6 = "fd00:0:1:0000::/56"
        DOMAIN = "bachert.it"
        HOSTNAMES = "postesel.bachert.it"
        POSTMASTER = "admin"
        TLS_FLAVOR = "mail"
        AUTH_RATELIMIT = "10/minute;1000/hour"
        DISABLE_STATISTICS = "True"
        ADMIN = "true"
        WEBMAIL = "none"
        WEBDAV = "none"
        ANTIVIRUS = "none"
        MESSAGE_SIZE_LIMIT = "50000000"
        FETCHMAIL_DELAY = "600"
        RECIPIENT_DELIMITER = "+"
        DMARC_RUA = "admin"
        DMARC_RUF = "admin"
        WEB_ADMIN = "/admin"
        PASSWORD_SCHEME = "BLF-CRYPT"
        REJECT_UNLISTED_RECIPIENT = "yes"
        LOG_LEVEL = "WARNING"
        DB_FLAVOR = "sqlite"
      }
    }

    task "front" {
      driver = "docker"

      volume_mount {
        volume      = "mailu-certs"
        destination = "/certs"
        read_only   = true
      }

      logs {
        max_files     = 1
        max_file_size = 1
      }

      config {
        image = "mailu/nginx:1.8"
        # image = "cbachert/nginx:local"
        ports = ["front_smtp", "front_http", "front_imap", "front_smtp_submission"]
        network_mode = "bridge"
      }

      resources {
        cpu    = 100
        memory = 32
      }

      env {
        ADMIN_ADDRESS = "[${NOMAD_IP_admin_http}]:${NOMAD_HOST_PORT_admin_http}"
        ANTISPAM_WEBUI_ADDRESS = "[${NOMAD_IP_rspamd_http}]:${NOMAD_HOST_PORT_rspamd_http}"
        
        #SECRET_KEY = ""
        SUBNET = "172.17.0.0/16"
        SUBNET6 = "fd00:0:1:0000::/56"
        DOMAIN = "bachert.it"
        HOSTNAMES = "postesel.bachert.it"
        POSTMASTER = "admin"
        TLS_FLAVOR = "mail"
        AUTH_RATELIMIT = "10/minute;1000/hour"
        DISABLE_STATISTICS = "True"
        ADMIN = "true"
        WEBMAIL = "none"
        WEBDAV = "none"
        ANTIVIRUS = "none"
        MESSAGE_SIZE_LIMIT = "50000000"
        FETCHMAIL_DELAY = "600"
        RECIPIENT_DELIMITER = "+"
        DMARC_RUA = "admin"
        DMARC_RUF = "admin"
        WEB_ADMIN = "/admin"
        PASSWORD_SCHEME = "BLF-CRYPT"
        REJECT_UNLISTED_RECIPIENT = "yes"
        LOG_LEVEL = "WARNING"
      }
    }
    
    service {
      name = "postesel-bachert-it-front-letsencrypt-dummy"
      tags = [
        "traefik.enable=true",
        "traefik.http.routers.postesel-bachert-it.rule=Host(`postesel.bachert.it`)",
        "traefik.http.routers.postesel-bachert-it.entrypoints=web,websecure",
        "traefik.http.routers.postesel-bachert-it.tls=true",
        "traefik.http.routers.postesel-bachert-it.tls.certresolver=le-prod" ]
    }

    task "imap" {
      driver = "docker"

      volume_mount {
        volume      = "mailu-mail"
        destination = "/mail"
        read_only   = false
      }

      logs {
        max_files     = 1
        max_file_size = 1
      }

      config {
        image = "mailu/dovecot:1.8"
        ports = ["imap_imap", "imap_lmtp"]
        volumes = [
          "local/overrides/dovecot:/overrides"
        ]
        network_mode = "bridge"
      }

      resources {
        cpu    = 100
        memory = 64
      }

      env {
        FRONT_ADDRESS = "[${NOMAD_IP_front_http}]:${NOMAD_HOST_PORT_front_http}"
        ADMIN_ADDRESS = "[${NOMAD_IP_admin_http}]:${NOMAD_HOST_PORT_admin_http}"
        ANTISPAM_WEBUI_ADDRESS = "[${NOMAD_IP_rspamd_http}]:${NOMAD_HOST_PORT_rspamd_http}"
        
        #SECRET_KEY = ""
        SUBNET = "172.17.0.0/16"
        SUBNET6 = "fd00:0:1:0000::/56"
        DOMAIN = "bachert.it"
        HOSTNAMES = "postesel.bachert.it"
        POSTMASTER = "admin"
        TLS_FLAVOR = "mail"
        AUTH_RATELIMIT = "10/minute;1000/hour"
        DISABLE_STATISTICS = "True"
        ADMIN = "true"
        WEBMAIL = "none"
        WEBDAV = "none"
        ANTIVIRUS = "none"
        MESSAGE_SIZE_LIMIT = "50000000"
        FETCHMAIL_DELAY = "600"
        RECIPIENT_DELIMITER = "+"
        DMARC_RUA = "admin"
        DMARC_RUF = "admin"
        WEB_ADMIN = "/admin"
        PASSWORD_SCHEME = "BLF-CRYPT"
        REJECT_UNLISTED_RECIPIENT = "yes"
        LOG_LEVEL = "DEBUG"
      }

      template {
        data = <<EOF
mail_nfs_index = yes
mail_nfs_storage = yes
mail_fsync = always
mmap_disable = yes
lock_method = dotlock
EOF
        destination = "local/overrides/dovecot/dovecot.conf"
      }
    }

    task "smtp" {
      driver = "docker"

      logs {
        max_files     = 1
        max_file_size = 1
      }

      config {
        image = "mailu/postfix:1.8"
        ports = ["smtp_smtp", "smtp_authsmtp"]
        network_mode = "bridge"
      }

      resources {
        cpu    = 100
        memory = 64
      }

      env {
        FRONT_ADDRESS = "[${NOMAD_IP_front_http}]:${NOMAD_HOST_PORT_front_http}"
        ADMIN_ADDRESS = "[${NOMAD_IP_admin_http}]:${NOMAD_HOST_PORT_admin_http}"
        ANTISPAM_MILTER_ADDRESS = "[${NOMAD_IP_rspamd_milter}]:${NOMAD_HOST_PORT_rspamd_milter}"
        LMTP_ADDRESS = "${NOMAD_IP_imap_lmtp}:${NOMAD_HOST_PORT_imap_lmtp}"

        #SECRET_KEY = ""
        SUBNET = "172.17.0.0/16"
        SUBNET6 = "fd00:0:1:0000::/56"
        DOMAIN = "bachert.it"
        HOSTNAMES = "${node.unique.name}.bachert.it"
        POSTMASTER = "admin"
        TLS_FLAVOR = "mail"
        AUTH_RATELIMIT = "10/minute;1000/hour"
        DISABLE_STATISTICS = "True"
        ADMIN = "true"
        WEBMAIL = "none"
        WEBDAV = "none"
        ANTIVIRUS = "none"
        MESSAGE_SIZE_LIMIT = "50000000"
        FETCHMAIL_DELAY = "600"
        RECIPIENT_DELIMITER = "+"
        DMARC_RUA = "admin"
        DMARC_RUF = "admin"
        WEB_ADMIN = "/admin"
        PASSWORD_SCHEME = "BLF-CRYPT"
        REJECT_UNLISTED_RECIPIENT = "yes"
        LOG_LEVEL = "WARNING"
      }
    }

    task "antispam" {
      driver = "docker"

      volume_mount {
        volume      = "mailu-filter"
        destination = "/var/lib/rspamd"
        read_only   = false
      }
      volume_mount {
        volume      = "mailu-dkim"
        destination = "/dkim"
        read_only   = false
      }

      logs {
        max_files     = 1
        max_file_size = 1
      }
      
      config {
        image = "mailu/rspamd:1.8"
        ports = ["rspamd_milter", "rspamd_http", "rspamd_fuzzy"]
        network_mode = "bridge"
      }
      
      resources {
        cpu    = 100
        memory = 96
      }

      env {
        HOST_FRONT = "127.0.0.1"   # does not seem to be actually used in container, but required by startup script
        REDIS_ADDRESS = "[${NOMAD_IP_redis}]:${NOMAD_HOST_PORT_redis}"
        SUBNET = "172.17.0.0/16"
      }
    }
  }
}