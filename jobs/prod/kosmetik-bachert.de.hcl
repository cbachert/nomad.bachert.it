job "kosmetik-bachert.de" {
  datacenters = ["dc1"]
  
  group "lighttpd" {
    count = 1

    ephemeral_disk {
      size      = "16"
    }
    
    volume "kosmetik-bachert.de-htdocs" {
      type      = "host"
      read_only = true
      source    = "kosmetik-bachert.de-htdocs"
    }

    network {
      mode = "bridge"
      port "http" { to = 80 }
    }

    task "lighttpd" {
      driver = "docker"

      volume_mount {
        volume      = "kosmetik-bachert.de-htdocs"
        destination = "/var/www/localhost/htdocs"
        read_only   = true
      }

      logs {
        max_files     = 1
        max_file_size = 1
      }

      config {
        image = "sebp/lighttpd"
        ports = ["http"]
        tty = true
        network_mode = "bridge"
        volumes = [
          "local/lighttpd.conf:/etc/lighttpd/lighttpd.conf"
        ]
      }

      template {
        data = <<EOF
server.use-ipv6 = "enable"
var.basedir  = "/var/www/localhost"
var.logdir   = "/var/log/lighttpd"
var.statedir = "/var/lib/lighttpd"
server.modules = (
    "mod_access",
    "mod_accesslog"
)
include "mime-types.conf"
server.username      = "lighttpd"
server.groupname     = "lighttpd"
server.document-root = var.basedir + "/htdocs"
server.pid-file      = "/run/lighttpd.pid"
server.errorlog      = "/dev/pts/0"
server.indexfiles    = ("index.php", "index.html",
                                                "index.htm", "default.htm")
server.follow-symlink = "enable"
static-file.exclude-extensions = (".php", ".pl", ".cgi", ".fcgi")
accesslog.filename   = "/dev/pts/0"
url.access-deny = ("~", ".inc")
server.network-backend = "writev"
EOF
destination = "local/lighttpd.conf"
      }

      resources {
        cpu    = 100
        memory = 50
      }
    }
    
    service {
      name = "kosmetik-bachert-de"
      port = "http"
      tags = [
        "traefik.enable=true",
        #"traefik.http.routers.kosmetik-bachert-de-http.rule=Host(`kosmetik-bachert.de`)",
        #"traefik.http.routers.kosmetik-bachert-de-http.entrypoints=web",
        "traefik.http.routers.kosmetik-bachert-de.rule=Host(`kosmetik-bachert.de`)",
        "traefik.http.routers.kosmetik-bachert-de.entrypoints=websecure",
        "traefik.http.routers.kosmetik-bachert-de.tls=true",
        "traefik.http.routers.kosmetik-bachert-de.tls.certresolver=le-prod",
      ]
    }
  }
}
