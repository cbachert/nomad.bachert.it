job "hcloud-ingress-dnslb" {
  region      = "global"
  datacenters = ["dc1"]
  type        = "service"

  group "traefik" {
    count = 1

    ephemeral_disk {
      size      = "16"
    }

    volume "traefik-acme" {
      type      = "host"
      read_only = false
      source    = "traefik-acme"
    }
    volume "traefik-acme-export" {
      type      = "host"
      read_only = false
      source    = "traefik-acme-export"
    }

    task "hcloud-dnslb-bachert-it" {
      driver = "raw_exec"
      config {
        command = "/opt/hcloud-dnsapi/hcloud-dnsapi-update.py"
      }

      logs {
        max_files     = 1
        max_file_size = 1
      }
       
      resources {
        cpu = 20
        memory = 10
      }

      lifecycle {
        hook    = "prestart"
      }

      env {
        HCLOUD_DNSAPI_UPDATE_ZONE_NAME = "bachert.it"
        HCLOUD_DNSAPI_UPDATE_RECORD_NAMES = "ingress"
      }
    }

    task "hcloud-dnslb-kosmetik-bachert-de" {
      driver = "raw_exec"
      config {
        command = "/opt/hcloud-dnsapi/hcloud-dnsapi-update.py"
      }

      logs {
        max_files     = 1
        max_file_size = 1
      }
       
      resources {
        cpu = 20
        memory = 10
      }

      lifecycle {
        hook    = "prestart"
      }

      env {
        HCLOUD_DNSAPI_UPDATE_ZONE_NAME = "kosmetik-bachert.de"
        HCLOUD_DNSAPI_UPDATE_RECORD_NAMES = "@"
      }
    }

    task "traefik" {
      driver = "docker"

      volume_mount {
        volume      = "traefik-acme"
        destination = "/etc/traefik/acme"
        read_only   = false
      }

      logs {
        max_files     = 1
        max_file_size = 1
      }

      config {
        image        = "traefik:2.3"
        network_mode = "host"

        volumes = [
          "local/traefik.toml:/etc/traefik/traefik.toml",
        ]

        # dns_servers = ["::1"]
      }

      template {
        data = <<EOF
# https://raw.githubusercontent.com/containous/traefik/master/traefik.sample.toml
[global]
  checkNewVersion = false
  sendAnonymousUsage = false
[entryPoints]
  #[entryPoints.tcp25]
  #  address = ":25"
  #[entryPoints.tcp143]
  #  address = ":143"
  #[entryPoints.tcp465]
  #  address = ":465"
  #[entryPoints.tcp587]
  #  address = ":587"
  [entryPoints.web]
    address = ":80"
    #[entryPoints.web.http]
    #  [entryPoints.web.http.redirections]
    #    [entryPoints.web.http.redirections.entryPoint]
    #      to = "websecure"
    #      scheme = "https"
  [entryPoints.websecure]
    address = ":443"
  [entryPoints.udp34197]
    address = ":34197/udp"
[log]
  level = "DEBUG"
[api]
  insecure = true
  dashboard = true
[providers.consulCatalog]
  # prefix = "traefik" # default
  exposedByDefault = false
  [providers.consulCatalog.endpoint]
    address = "http://[::1]:8500"
    #domain = "consul.localhost"
[certificatesResolvers.le-prod.acme]
  email = "root@bachert.it"
  storage = "/etc/traefik/acme/acme.json"
  [certificatesResolvers.le-prod.acme.tlsChallenge]
[certificatesResolvers.le-staging.acme]
  email = "root@bachert.it"
  storage = "/etc/traefik/acme/acme-staging.json"
  caServer = "https://acme-staging-v02.api.letsencrypt.org/directory"
  [certificatesResolvers.le-staging.acme.tlsChallenge]
EOF
        destination = "local/traefik.toml"
      }

      resources {
        cpu    = 100
        memory = 128

        network {
          mbits = 10

          port "http" {
            static = 8080
          }

          port "api" {
            static = 8081
          }
        }
      }

      service {
        name = "traefik"

        check {
          name     = "alive"
          type     = "tcp"
          port     = "http"
          interval = "10s"
          timeout  = "2s"
        }
        tags = [
          "traefik.enable=true",
          "traefik.http.middlewares.redirect-to-https.redirectscheme.scheme=https",
          "traefik.http.middlewares.redirect-to-https.redirectscheme.permanent=true",
          "traefik.http.routers.http-catchall.rule=hostregexp(`{host:.+}`)",
          "traefik.http.routers.http-catchall.entrypoints=web",
          "traefik.http.routers.http-catchall.middlewares=redirect-to-https",
        ]
      }
    }

    task "traefik-certdumper" {
      driver = "docker"

      volume_mount {
        volume      = "traefik-acme"
        destination = "/traefik"
        read_only   = true
      }
      volume_mount {
        volume      = "traefik-acme-export"
        destination = "/output"
        read_only   = false
      }

      logs {
        max_files     = 1
        max_file_size = 1
      }
      
      config {
        image = "mailu/traefik-certdumper:1.8"
      }
      
      resources {
        cpu    = 100
        memory = 16
      }

      env {
        TRAEFIK_VERSION = "v2"
        DOMAIN = "postesel.bachert.it"
      }
    }
  }
}