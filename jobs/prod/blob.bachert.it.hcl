job "blob.bachert.it" {
  datacenters = ["dc1"]
  
  group "lighttpd" {
    count = 1
    
    volume "blob.bachert.it-htdocs" {
      type      = "host"
      read_only = true
      source    = "blob.bachert.it-htdocs"
    }

    volume "blob.bachert.it-config" {
      type      = "host"
      read_only = true
      source    = "blob.bachert.it-config"
    }

    network {
      port "http" { to = 80 }
      # mode = "bridge"
    }

    task "lighttpd" {
      driver = "docker"

      volume_mount {
        volume      = "blob.bachert.it-htdocs"
        destination = "/var/www/localhost/htdocs"
        read_only   = true
      }

      volume_mount {
        volume      = "blob.bachert.it-config"
        destination = "/etc/lighttpd"
        read_only   = true
      }

      config {
        image = "sebp/lighttpd"
        #args = [ 
        #  "-D"
        #]
        ports = ["http"]
        tty = true
      }

      resources {
        cpu    = 100
        memory = 50
      }
    }
    
    service {
      name = "blob-bachert-it"
      port = "http"
      tags = [
        "traefik.enable=true",
        "traefik.http.routers.blob-bachert-it.rule=PathPrefix(`/test`)",
        "traefik.http.routers.blob-bachert-it.rule=Host(`blob.bachert.it`)",
      ]
    }
  }
}