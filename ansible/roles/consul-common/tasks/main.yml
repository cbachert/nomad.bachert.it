---
- name: consul - install unzip
  apt:
    name: unzip
    update_cache: yes
    cache_valid_time: 3600
- name: consul - download consul binary
  get_url:
    url: "{{ 'https://releases.hashicorp.com/consul/' + consul_version + '/consul_' + consul_version + '_linux_amd64.zip' }}"
    dest: /tmp/consul.zip
    force: no
    checksum: "{{ consul_checksum }}"
- name: consul - unzip consul binary
  unarchive:
    src: /tmp/consul.zip
    dest: /usr/local/bin/
    remote_src: yes
    owner: root
    mode: "0755"
  notify:
    - systemd_restart_consul
- name: consul - create unique, non-privileged system user
  user:
    name: consul
    home: /etc/consul.d
    shell: /bin/false
    system: yes
- name: consul - create config directory
  file:
    path: /etc/consul.d
    state: directory
    owner: consul
    group: consul
    mode: "0755"
- name: consul - Check if secret key file already exists
  stat:
    path: "/etc/consul.d/consul_encrypt.key"
  register: consul_encrypt_stat
  run_once: true
- name: consul - Generate random secret key on localhost
  set_fact:
    consul_encrypt_random_b64: "{{ lookup('password', '/dev/null length=32') | b64encode }}"
  delegate_to: localhost
  delegate_facts: true
  when: not consul_encrypt_stat.stat.exists
- name: consul - Write secret key if secret key file does not exist
  copy: 
    content: "{{ hostvars['localhost']['consul_encrypt_random_b64'] }}"
    dest: "/etc/consul.d/consul_encrypt.key"   
    mode: "0400"
  when: not consul_encrypt_stat.stat.exists
- name: consul - Read secret key
  slurp:
    src: /etc/consul.d/consul_encrypt.key
  register: consul_encrypt_slurp
- name: consule - Read secret key content
  set_fact:
    consul_encrypt: "{{consul_encrypt_slurp.content | b64decode}}"
- name: consul - template common configuration file
  template:
    src: etc-consul.d-consul.hcl.j2
    dest: /etc/consul.d/consul.hcl
    owner: consul
    group: consul
    mode: "0600"
  notify:
    - systemd_restart_consul
- name: consul - create data directory
  file:
    path: /opt/consul
    state: directory
    owner: consul
    group: root
    mode: 0755
- name: consul - create directory for systemd service file
  file:
    path: /usr/local/lib/systemd/system
    state: directory
    owner: root
    group: root
    mode: 0755
- name: consul - copy systemd service file
  copy:
    src: etc-systemd-system-consul.service
    dest: /usr/local/lib/systemd/system/consul.service
    owner: root
    group: root
    mode: '0644'
  notify:
    - systemd_restart_consul
- name: consul - create symlink for systemd service file
  file:
    src: "/usr/local/lib/systemd/system/consul.service"
    dest: "/etc/systemd/system/consul.service"
    state: link
  notify:
    - systemd_restart_consul
- name: consul - enable systemd service
  systemd:
    name: consul
    enabled: yes
    state: started