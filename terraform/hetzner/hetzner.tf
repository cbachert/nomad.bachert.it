terraform {
  required_version = ">= 0.13"
  required_providers {
    hcloud = {
      source = "hetznercloud/hcloud"
      version = "~> 1.23.0"
    }
    hetznerdns = {
      source = "timohirt/hetznerdns"
      version = "1.0.6"
    }
    local = {
      version = "~> 2.0.0"
    }
    null = {
      version = "~> 3.0.0"
    }
    template = {
      version = "~> 2.2.0"
    }
  }
}

# Set the variable value in *.tfvars file
# or using the -var="hcloud_token=..." CLI option
# or using "export TF_VAR_hcloud_token=..." env variable
variable "hcloud_token" {}

# Set the variable value in *.tfvars file
# or using the -var="hetznerdns_token=..." CLI option
# or using "export TF_VAR_hetznerdns_token=..." env variable
variable "hetznerdns_token" {}

# Configure the Hetzner Cloud Provider
provider "hcloud" {
  token = var.hcloud_token
}

provider "hetznerdns" {
  apitoken = var.hetznerdns_token
}

data "hcloud_location" "nbg1" {
  name = "nbg1"
}

data "hcloud_image" "ubuntu-2004" {
  name = "ubuntu-20.04"
}

data "hcloud_ssh_keys" "all" {
}

variable "hcloud_server_type" {
  default = "cx11"
}

variable "server_count" {
  default = 0
}

variable "client_count" {
  default = 0
}

variable "client_server_count" {
  default = 3
}

resource "hcloud_server" "server" {
  count = var.server_count
  name  = "nomads${count.index + 1}"
  image = data.hcloud_image.ubuntu-2004.name
  server_type = var.hcloud_server_type
  location = data.hcloud_location.nbg1.name
  ssh_keys = data.hcloud_ssh_keys.all.ssh_keys.*.name
}

resource "hcloud_server" "client" {
  count = var.client_count
  name  = "nomadc${count.index + 1}"
  image = data.hcloud_image.ubuntu-2004.name
  server_type = var.hcloud_server_type
  location = data.hcloud_location.nbg1.name
  ssh_keys = data.hcloud_ssh_keys.all.ssh_keys.*.name
}

resource "hcloud_server" "clientserver" {
  count = var.client_server_count
  name  = "nomad${count.index + 1}"
  image = data.hcloud_image.ubuntu-2004.name
  server_type = var.hcloud_server_type
  location = data.hcloud_location.nbg1.name
  ssh_keys = data.hcloud_ssh_keys.all.ssh_keys.*.name
}

data "template_file" "ansible_inventory" {
  template = file("tf_ansible_inventory.tpl") 
  vars = {
    servers = join("\n", [for instance in hcloud_server.server : join("", [instance.name, " ansible_host=", instance.ipv6_address, " host_ext=", instance.ipv6_address, " host_extv4=", instance.ipv4_address, " host_extv6=", instance.ipv6_address ])] )
    clients = join("\n", [for instance in hcloud_server.client : join("", [instance.name, " ansible_host=", instance.ipv6_address, " host_ext=", instance.ipv6_address, " host_extv4=", instance.ipv4_address, " host_extv6=", instance.ipv6_address ])] )
    clientservers = join("\n", [for instance in hcloud_server.clientserver : join("", [instance.name, " ansible_host=", instance.ipv6_address, " host_ext=", instance.ipv6_address, " host_extv4=", instance.ipv4_address, " host_extv6=", instance.ipv6_address ])] )
  }
}

resource "local_file" "ansible_inventory" {
  content  = data.template_file.ansible_inventory.rendered
  filename = "./output/tf_ansible_inventory"
}

# rDNS
resource "hcloud_rdns" "clientserverv" {
  count = var.client_server_count
  server_id = hcloud_server.clientserver[count.index].id
  ip_address = hcloud_server.clientserver[count.index].ipv6_address
  dns_ptr = "${hcloud_server.clientserver[count.index].name}.bachert.it"
}

resource "hcloud_rdns" "clientserverv4" {
  count = var.client_server_count
  server_id = hcloud_server.clientserver[count.index].id
  ip_address = hcloud_server.clientserver[count.index].ipv4_address
  dns_ptr = "${hcloud_server.clientserver[count.index].name}.bachert.it"
}

# DNS
resource "hetznerdns_zone" "bachert_it" {
  name = "bachert.it"
  ttl  = 3600
}

resource "hetznerdns_record" "bachert_it-NS1" {
  zone_id = hetznerdns_zone.bachert_it.id
  name    = "@"
  value   = "hydrogen.ns.hetzner.com."
  type    = "NS"
  ttl     = 3600
}

resource "hetznerdns_record" "bachert_it-NS2" {
  zone_id = hetznerdns_zone.bachert_it.id
  name    = "@"
  value   = "oxygen.ns.hetzner.com."
  type    = "NS"
  ttl     = 3600
}

resource "hetznerdns_record" "bachert_it-NS3" {
  zone_id = hetznerdns_zone.bachert_it.id
  name    = "@"
  value   = "helium.ns.hetzner.de."
  type    = "NS"
  ttl     = 3600
}

resource "hetznerdns_record" "bachert_it-MX" {
  zone_id = hetznerdns_zone.bachert_it.id
  name    = "@"
  value   = "10 mx01"
  type    = "MX"
  ttl     = 3600
}

resource "hetznerdns_record" "bachert_it-TXT1" {
  zone_id = hetznerdns_zone.bachert_it.id
  name    = "@"
  value   = "\"google-site-verification=_6ihLRqXYQom0SVsSeQRRxfxTf5vLDoICHVFFRQ0iT0\""
  type    = "TXT"
  ttl     = 3600
}

resource "hetznerdns_record" "bachert_it-TXT2" {
  zone_id = hetznerdns_zone.bachert_it.id
  name    = "@"
  value   = "\"v=spf1 mx -all\""
  type    = "TXT"
  ttl     = 3600
}

resource "hetznerdns_record" "clientserver" {
  count = var.client_server_count
  zone_id = hetznerdns_zone.bachert_it.id
  name    = hcloud_server.clientserver[count.index].name
  value   = hcloud_server.clientserver[count.index].ipv6_address
  type    = "AAAA"
  ttl     = 3600
}

resource "hetznerdns_record" "clientserverv4" {
  count = var.client_server_count
  zone_id = hetznerdns_zone.bachert_it.id
  name    = hcloud_server.clientserver[count.index].name
  value   = hcloud_server.clientserver[count.index].ipv4_address
  type    = "A"
  ttl     = 3600
}

resource "hetznerdns_record" "dkim__domainkey_bachert_it-TXT" {
  zone_id = hetznerdns_zone.bachert_it.id
  name    = "dkim._domainkey"
  value   = "\"v=DKIM1; k=rsa; p=MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA6VDAslZfIxEj0jrv0FZy9AMfaoak9PLbQ2hAOn7oHvkFXEGrQyQS4jXBSav4CjymD9OR+OPvce6cdaNvJOS8NLN1Iz8hfNyUXDhQ2C0f0OEiAwLzuUBZyVdkGSMmqWqA5js4YxOqAigREXThJ7e6HYudRqZF4y2PcpYvF0WhSmgGEnlUk+w0IrDdNdisk/pmW\" \"BQDLmOy2Ln9sGNs1xvLQQjv30hrhQhvKQZFg254zOk+Gmk4xG9M9nc4aLleNWbXebHBCo3EzWvA6eLUrDVLthbHtGBMF8OdPV+uTBVyV+LI1l0q/ioztovpP4EBTtiGjEA34a4K1CAwz6K4b26FeQIDAQAB\" "
  type    = "TXT"
  ttl     = 3600
}

resource "hetznerdns_record" "_dmarc_bachert_it-TXT" {
  zone_id = hetznerdns_zone.bachert_it.id
  name    = "_dmarc"
  value   = "\"v=DMARC1; p=reject; rua=mailto:root@bachert.it\""
  type    = "TXT"
  ttl     = 3600
}
