terraform {
  required_version = ">= 0.13"
  required_providers {
    libvirt = {
      source  = "dmacvicar/libvirt"
      version = "0.6.2"
    }
  }
}

provider "libvirt" {
  uri = "qemu:///system"
}

resource "libvirt_volume" "ubuntu2004-base" {
  name = "focal-server-cloudimg-amd64.img"
  pool = "default"
  source = "../../../../../VM/libvirt/src/focal-server-cloudimg-amd64.img"
  format = "qcow2"
}

variable "master_count" {
  default = 0
}

variable "worker_count" {
  default = 0
}

variable "client_server_count" {
  default = 3
}

resource "libvirt_volume" "cloud-init-master" {
  count = var.master_count
  name = "cloud-init-master${count.index + 1}.iso"
  pool = "libvirt"
  source = "../../../../../VM/libvirt/src/config.iso"
}

resource "libvirt_volume" "cloud-init-worker" {
  count = var.worker_count
  name = "cloud-init-worker${count.index + 1}.iso"
  pool = "libvirt"
  source = "../../../../../VM/libvirt/src/config.iso"
}

resource "libvirt_volume" "cloud-init-client_server" {
  count = var.client_server_count
  name = "cloud-init-client_server${count.index + 1}.iso"
  pool = "libvirt"
  source = "../../../../../VM/libvirt/src/config.iso"
}

# volume to attach to the "master" domain as main disk
resource "libvirt_volume" "vm-master" {
  count = var.master_count
  name = "vm-master${count.index + 1}.qcow2"
  pool = "default"
  base_volume_id = libvirt_volume.ubuntu2004-base.id
  size = "10737418240"
}

resource "libvirt_volume" "vm-worker" {
  count = var.worker_count
  name = "vm-worker${count.index + 1}.qcow2"
  pool = "default"
  base_volume_id = libvirt_volume.ubuntu2004-base.id
  size = "10737418240"
}

resource "libvirt_volume" "client_server" {
  count = var.client_server_count
  name = "client_server${count.index + 1}.qcow2"
  pool = "default"
  base_volume_id = libvirt_volume.ubuntu2004-base.id
  size = "10737418240"
}

# Define KVM domain to create
resource "libvirt_domain" "vm-master" {
  count = var.master_count
  name   = "vm-master${count.index + 1}"
  memory = "768"
  vcpu   = 1

  network_interface {
    network_name = "default"
    wait_for_lease = true
  }

  disk {
    volume_id = libvirt_volume.vm-master[count.index].id
  }

  disk {
    volume_id = libvirt_volume.cloud-init-master[count.index].id
  }

  console {
    type = "pty"
    target_type = "serial"
    target_port = "0"
  }

  graphics {
    type = "spice"
    listen_type = "address"
    autoport = true
  }
}

# Define KVM domain to create
resource "libvirt_domain" "vm-worker" {
  count = var.worker_count
  name   = "vm-worker${count.index + 1}"
  memory = "768"
  vcpu   = 1

  network_interface {
    network_name = "default"
    wait_for_lease = true
  }

  disk {
    volume_id = libvirt_volume.vm-worker[count.index].id
  }

  disk {
    volume_id = libvirt_volume.cloud-init-worker[count.index].id
  }

  console {
    type = "pty"
    target_type = "serial"
    target_port = "0"
  }

  graphics {
    type = "spice"
    listen_type = "address"
    autoport = true
  }
}


# Define KVM domain to create
resource "libvirt_domain" "client_server" {
  count = var.client_server_count
  name   = "nomad${count.index + 1}"
  memory = "1024"
  vcpu   = 1

  network_interface {
    network_name = "default"
    wait_for_lease = true
  }

  disk {
    volume_id = libvirt_volume.client_server[count.index].id
  }

  disk {
    volume_id = libvirt_volume.cloud-init-client_server[count.index].id
  }

  console {
    type = "pty"
    target_type = "serial"
    target_port = "0"
  }

  graphics {
    type = "spice"
    listen_type = "address"
    autoport = true
  }
}

data "template_file" "ansible_inventory" {
  template = file("tf_ansible_inventory.tpl") 
  # VMs have to be running, otherwise the libvirt provider does not report the addresses and Terraform fails
  vars = {
    servers = join("\n", [for instance in libvirt_domain.vm-master : join("", [instance.name, " ansible_host=", instance.network_interface[0].addresses[0], " host_ext=", instance.network_interface[0].addresses[0], " host_extv4=", instance.network_interface[0].addresses[0] ])] )
    clients = join("\n", [for instance in libvirt_domain.vm-worker : join("", [instance.name, " ansible_host=", instance.network_interface[0].addresses[0], " host_ext=", instance.network_interface[0].addresses[0], " host_extv4=", instance.network_interface[0].addresses[0] ])] )
    clientservers = join("\n", [for instance in libvirt_domain.client_server : join("", [instance.name, " ansible_host=", instance.network_interface[0].addresses[0], " host_ext=", instance.network_interface[0].addresses[0], " host_extv4=", instance.network_interface[0].addresses[0] ])] )
  }
}

resource "local_file" "ansible_inventory" {
  content  = data.template_file.ansible_inventory.rendered
  filename = "./output/tf_ansible_inventory"
}

# resource "null_resource" "cluster" {
#   # Changes to any instance of the cluster requires re-provisioning
#   triggers = {
#     servers = join("\n", [for instance in libvirt_domain.vm-master : join("", [instance.name, " ansible_host=", instance.network_interface[0].addresses[0], " host_ext=", instance.network_interface[0].addresses[0], " host_extv4=", instance.network_interface[0].addresses[0] ])] )
#     clients = join("\n", [for instance in libvirt_domain.vm-worker : join("", [instance.name, " ansible_host=", instance.network_interface[0].addresses[0], " host_ext=", instance.network_interface[0].addresses[0], " host_extv4=", instance.network_interface[0].addresses[0] ])] )
#     clientservers = join("\n", [for instance in libvirt_domain.client_server : join("", [instance.name, " ansible_host=", instance.network_interface[0].addresses[0], " host_ext=", instance.network_interface[0].addresses[0], " host_extv4=", instance.network_interface[0].addresses[0] ])] )
#   }
# 
#   provisioner "local-exec" {
#     command = "ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -u adm_christian -i ./output/tf_ansible_inventory ../../ansible/ansible-provision-nomad.yml"
#   }
# }
